
import com.ideas.casper.common.entity.{CLASS_NAME};
#if (${EMBED_CLASS} && ${EMBED_CLASS} != "") 
import com.ideas.casper.common.entity.${EMBED_CLASS};
#end
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ${CLASS_NAME}Repository extends CrudRepository<${CLASS_NAME}, #if (${EMBED_CLASS} && ${EMBED_CLASS} != "") ${EMBED_CLASS} #else Integer#end> {
}