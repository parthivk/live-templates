import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
#parse("File Header.java")
#set ($class = ${NAME})

#set ($className = $class.toString().substring(0, $class.toString().lastIndexOf("Test")))

@ExtendWith(MockitoExtension.class)
class ${NAME} {
    @InjectMocks
    private $className ${NAME};

  ${BODY}
}