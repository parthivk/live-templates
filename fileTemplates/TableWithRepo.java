package ${PACKAGE_NAME};

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
#if (${TABLE_NAME} && ${TABLE_NAME} != "")
@Table(name = "${TABLE_NAME}")
#end
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ${CLASS_NAME} implements Serializable {
    
    #if (${EMBED_CLASS} && ${EMBED_CLASS} != "")
    @EmbeddedId
    private ${EMBED_CLASS} id;
    #end
    
}