For anyone wishing to use these settings for intellij. go to File -> Manage IDE settings -> settings Repository enter the repository name as follows

https://<bitbucket_username>@bitbucket.org/parthivk/live-templates.git

and click on overwrite local.


For anyone wishing to selectively import these settings into their intellij files, currently, intellij doesn't provide an automated solution for that.
But one can still do that , by copying select template file from this repository to the intellij config file location '%APPDATA%\JetBrains\IntelliJIdea2022.2\jba_config\templates'

	1. templates/SQL.xml
	2. templates/Java.xml
	3. templates/Java Testing.xml
	4. postfixTemplates.xml
	5. fileTemplates/*
	
